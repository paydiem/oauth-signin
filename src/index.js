import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import 'bootstrap/dist/css/bootstrap.css';
import registerServiceWorker from './registerServiceWorker';
import { appRoutes } from './routes/routes'

const routes = appRoutes();

ReactDOM.render(
    routes, 
    document.getElementById('root')
);

registerServiceWorker();
