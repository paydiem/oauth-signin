import React, { Component } from 'react';
import { Navbar, Button } from 'react-bootstrap';
import '../styles/app.css';

class App extends Component {
  goTo = (route) => {
    this.props.history.replace(`/${route}`);
  };

  login = () => {
    this.props.auth.login();
  };

  logout = () => {
    this.props.auth.logout();
  }

  render() {
    const { isAuthenticated } = this.props.auth;

    return (
      <div>
        <Navbar fluid>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#">Paydiem - Flash Debit</a>
            </Navbar.Brand>
            <Button
              bsStyle="primary"
              className="btn-margin"
              onClick={() => this.goTo('home')} 
            >
              Home
            </Button>
            {
              !isAuthenticated() && (
                <Button
                  id="pdLoginBtn"
                  bsStyle="primary"
                  className="btn-margin"
                  onClick={() => this.login()}
                >
                  Log In
                </Button>
              )
            }
            {
              isAuthenticated() && (
                <Button
                  id="pdLogoutBtn"
                  bsStyle="primary"
                  className="btn-margin"
                  onClick={() => this.logout()}
                >
                  Log Out
                </Button>
              )
            }
          </Navbar.Header>
        </Navbar>
      </div>
    );
  }
}

export default App;